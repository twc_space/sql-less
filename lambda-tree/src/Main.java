import co.streamx.fluent.extree.expression.LambdaExpression;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    static UserMapper userMapper = new UserMapperI();

    public static void main(String[] args) {
        HashMap<String, Object> map = new HashMap<>();

        Main.wrap(() -> userMapper.get(map));

    }

    public static void wrap(VoidFunction voidFunction){
        LambdaExpression<VoidFunction> expression = LambdaExpression.parse(voidFunction);
        expression.getBody();
    }

}



interface UserMapper {
    List<String> get(Map<String, Object> a);
}

class UserMapperI implements UserMapper {
    public List<String> get(Map<String, Object> a) {
        return Collections.singletonList((String) a.get("a"));
    }
}




