//package org.twc.sqlless;
//
//import org.twc.sqlless.core.*;
//import org.twc.sqlless.core.statement.CUD;
//import org.twc.sqlless.core.statement.Deleter.Delete;
//import org.twc.sqlless.core.statement.Inserter.Insert;
//import org.twc.sqlless.core.statement.Queryer.Query;
//import org.twc.sqlless.core.statement.Queryer.Query2;
//import org.twc.sqlless.core.statement.Queryer.Query3;
//import org.twc.sqlless.core.statement.Queryer.Query4;
//import org.twc.sqlless.core.statement.Updater.Update;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.database.ResolveManager;
//
//import java.util.List;
//
///**
// * sql builder
// *
// * @author twc
// * @date 2023/11/24
// */
//public final class SQLLess {
//
//    private final static Resolve<SqlClient<?>> resolve = new ResolveManager();
//
//    private SQLLess() {}
//
//    public static <T> Query<T> query(Class<T> c1) {
//        return new Query<>(c1, resolve);
//    }
//
//    public static <T1, T2> Query2<T1, T2> query(Class<T1> c1, Class<T2> c2) {
//        return new Query2<>(null, null, c1, c2, resolve);
//    }
//
//    public static <T1, T2, T3> Query3<T1, T2, T3> query(Class<T1> c1, Class<T2> c2, Class<T3> c3) {
//        return new Query3<>(null, null, c1, c2, c3, resolve);
//    }
//
//    public static <T1, T2, T3, T4> Query4<T1, T2, T3, T4> query(Class<T1> c1, Class<T2> c2, Class<T3> c3, Class<T4> c4) {
//        return new Query4<>(null, null, c1, c2, c3, c4, resolve);
//    }
//
//    public static <T> Insert<T> insert(Class<T> c1) {
//        return new Insert<>(c1, resolve);
//    }
//
//    public static <T> Delete<T> delete(Class<T> c1) {
//        return new Delete<>(c1, resolve);
//    }
//
//    public static <T> Update<T> update(Class<T> c1) {
//        return new Update<>(c1, resolve);
//    }
//
//    /****************************************************************************************************/
//
//    public static <T> boolean save(T t) {
//        Entity entity = resolve.save(t);
//        return resolve.getClient().startUpdate(entity) == 1;
//    }
//
//    public static <T> boolean save(List<T> ts) {
//        if (!ts.isEmpty()) {
//            List<Entity> entityList = resolve.batchSave(ts);
//            List<Integer> array = resolve.getClient().batchUpdate(entityList);
//            int count = 0;
//            for (int i : array) count += i;
//            return count == ts.size();
//        } else {
//            return false;
//        }
//    }
//
//    /************************************************************************************************************/
//    public static Transaction startTransaction(CUD<?>... cuds) {
//        Transaction transaction = new Transaction();
//        for (CUD<?> c : cuds) {
//            transaction.push(c);
//        }
//        return transaction;
//    }
//
//    public static Transaction begin(CUD<?>... cuds) {
//        Transaction transaction = new Transaction();
//        for (CUD<?> c : cuds) {
//            transaction.push(c);
//        }
//        return transaction;
//    }
//
//    /**********************************************************************************************************/
//
//    public static ResolveManager register(String resolveName, Resolve<SqlClient<?>> resolve) {
//        ResolveManager manager = (ResolveManager) SQLLess.resolve;
//        return manager.register(resolveName, resolve);
//    }
//
//    public static ResolveManager unregister(String resolveName) {
//        ResolveManager manager = (ResolveManager) SQLLess.resolve;
//        return manager.unregister(resolveName);
//    }
//
//    public static void current(String name) {
//        ResolveManager.current(name);
//    }
//
//    public static ResolveManager defaultName(String name) {
//        ResolveManager manager = (ResolveManager) SQLLess.resolve;
//        return manager.setDefault(name);
//    }
//
//}
//
