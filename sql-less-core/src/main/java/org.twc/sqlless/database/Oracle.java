//package org.twc.sqlless.database;
//
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.core.SqlClient;
//
///**
// * Oracle  sql 解析器
// *
// * @author twc
// * @date 2023/11/24
// */
//public class Oracle extends Mysql implements Resolve<SqlClient<?>> {
//
//    public Oracle(SqlClient<?> sqlClient) {
//        super(sqlClient);
//    }
//
//    @Override
//    public Type type() {
//        return Type.Oracle;
//    }
//
//
//}
