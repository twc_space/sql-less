package org.twc.sqlless.database;

import org.twc.sqlless.core.SqlClient;
import org.twc.sqlless.core.func.Func0;
import org.twc.sqlless.expression.NewExpression;

import java.util.List;
import java.util.Map;

/**
 * noop 执行器
 *
 * @author twc
 * @date 2023/11/25
 */
public class NOOPSqlClient implements SqlClient<Void> {

    @Override
    public void setSource(Void source) {}

    @Override
    public Void getSource() { return null; }

    @Override
    public <R> List<R> startQuery(Entity entity, NewExpression<R> newExpression) { return null; }

    @Override
    public <Key, R> Map<Key, R> startQuery(Entity entity, NewExpression<R> newExpression, Func0<R, Key> getKey) { return null; }

    @Override
    public <Key, Value, R> Map<Key, Value> startQuery(Entity entity, NewExpression<R> newExpression, Func0<R, Key> getKey, Func0<R, Value> getValue) { return null; }

    @Override
    public int startUpdate(Entity entity) { return 0; }

    @Override
    public List<Integer> batchUpdate(List<Entity> entityList) { return null; }

    @Override
    public boolean transactionCud(List<Entity> entityList, Integer transactionIsolation) { return false; }

}
