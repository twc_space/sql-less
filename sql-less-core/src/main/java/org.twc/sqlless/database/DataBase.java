package org.twc.sqlless.database;


/**
 * 数据库
 *
 * @author twc
 * @date 2023/11/25
 */
public interface DataBase {

    Type type();


    enum Type {
        Mysql,
        Oracle;
    }
}
