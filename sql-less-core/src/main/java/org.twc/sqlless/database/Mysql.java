//package org.twc.sqlless.database;
//
//import org.twc.sqlless.core.*;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.statement.Deleter.Delete;
//import org.twc.sqlless.core.statement.Inserter.Insert;
//import org.twc.sqlless.core.statement.Statement;
//import org.twc.sqlless.core.statement.Updater.Update;
//import org.twc.sqlless.expression.*;
//
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * Mysql sql 解析器
// *
// * @author twc
// * @date 2023/11/24
// */
//public class Mysql implements Resolve<SqlClient<?>>, DataBase {
//
//    private final SqlClient<?> sqlClient;
//
//    public Mysql(SqlClient<?> sqlClient) {
//        this.sqlClient = sqlClient;
//    }
//
//    @Override
//    public SqlClient<?> getClient() {
//        return this.sqlClient;
//    }
//
//    public Entity query(boolean isDistinct, List<Base> bases, NewExpression<?> newExpression, List<Class<?>> queryClasses, List<?> queryTarget, List<Class<?>> joins) {
//        Entity entity = Entity.create();
//        select(entity, isDistinct, newExpression, queryClasses, queryTarget, joins);
//        for (Base base : bases) {
//            if (base instanceof Where) {
//                where(entity, ((Where) base).getExpression(), queryTarget);
//            } else if (base instanceof Take) {
//                take(entity, (Take) base);
//            } else if (base instanceof Skip) {
//                skip(entity, (Skip) base);
//            } else if (base instanceof OrderBy) {
//                orderBy(entity, (OrderBy) base, queryTarget);
//            } else if (base instanceof Join) {
//                join(entity, (Join) base, queryClasses);
//            } else if (base instanceof On) {
//                on(entity, ((On) base).getExpression(), queryTarget);
//            }
//        }
//        return entity;
//    }
//
//    public Entity cud(Statement<?> statement) {
//        if (statement instanceof Insert) {
//            return insert(statement);
//        } else if (statement instanceof Delete) {
//            return delete(statement);
//        } else if (statement instanceof Update) {
//            return update(statement);
//        }
//        return Entity.create();
//    }
//
//    public Entity insert(Statement<?> statement) {
//        Entity entity = Entity.create();
//        List<?> targets = statement.getQueryTargets();
//        entity.append("insert into ").append(ClassDefinitionCache.getTableName(targets.get(0).getClass())).append(" ");
//        for (Base base : statement.getBases()) {
//            if (base instanceof SetData) {
//                setInsert(entity, (SetData) base, targets);
//            }
//        }
//        return entity;
//    }
//
//    public <T> Entity save(T save) {
//        Entity entity = Entity.create();
//        entity.append("insert into ").append(ClassDefinitionCache.getTableName(save.getClass()))
//                .append(" ").append("set").append(" ");
//        Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(save.getClass());
//        List<Field> fields = ClassDefinitionCache.getTypeFields(save.getClass());
//        int count = 0;
//        for (Field field : fields) {
//            if (field.isAnnotationPresent(Id.class)) {
//                switch (field.getAnnotation(GeneratedValue.class).strategy()) {
//                    case IDENTITY:
//                        continue;
//                    case TABLE:
//                    case SEQUENCE:
//                    case AUTO:
//                }
//            }
//            try {
//                Object o = field.get(save);
//                if (o != null) {
//                    entity.append(map.get(field.getName())).append(" = ").append("?").append(",");
//                    entity.values.add(o);
//                    count++;
//                }
//            } catch (IllegalAccessException e) {
//                throw new RuntimeException(e);
//            }
//        }
//        if (count > 0) entity.deleteLast();
//        return entity;
//    }
//
//    public Entity delete(Statement<?> statement) {
//        Entity entity = Entity.create();
//        entity.append("delete").append(" ").append(Resolve.alias(0))
//                .append(".*").append(" ").append("from").append(" ")
//                .append(ClassDefinitionCache.getTableName(statement.getQueryClasses().get(0))).append(" ")
//                .append("as").append(" ").append(Resolve.alias(0)).append(" ");
//        for (Base base : statement.getBases()) {
//            if (base instanceof Where) {
//                where(entity, ((Where) base).getExpression(), statement.getQueryTargets());
//            }
//        }
//        return entity;
//    }
//
//    public Entity update(Statement<?> statement) {
//        Entity entity = Entity.create();
//        List<?> targets = statement.getQueryTargets();
//        entity.append("update ").append(ClassDefinitionCache.getTableName(targets.get(0).getClass())).append(" ")
//                .append("as").append(" ").append(Resolve.alias(0)).append(" ");
//        for (Base base : statement.getBases()) {
//            if (base instanceof SetData) {
//                setUpdate(entity, (SetData) base, statement.getQueryTargets());
//            } else if (base instanceof Where) {
//                where(entity, ((Where) base).getExpression(), statement.getQueryTargets());
//            }
//        }
//        return entity;
//    }
//
//    public <T> List<Entity> batchSave(List<T> ts) {
//        List<Entity> entityList = new ArrayList<>(ts.size());
//        Class<?> type = ts.get(0).getClass();
//        String tableName = ClassDefinitionCache.getTableName(type);
//        Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(type);
//        List<Field> fields = ClassDefinitionCache.getTypeFields(type);
//        for (T t : ts) {
//            Entity entity = Entity.create();
//            entity.append("insert into ").append(tableName).append(" ")
//                    .append("set").append(" ");
//            for (Field field : fields) {
//                if (field.isAnnotationPresent(Id.class)) {
//                    switch (field.getAnnotation(GeneratedValue.class).strategy()) {
//                        case IDENTITY:
//                            continue;
//                        case TABLE:
//                        case SEQUENCE:
//                        case AUTO:
//                    }
//                }
//                try {
//                    entity.append(map.get(field.getName())).append(" = ");
//                    Object o = field.get(type);
//                    if (o != null) {
//                        entity.pushValue(o);
//                    } else {
//                        entity.append("null");
//                    }
//                    entity.append(",");
//                } catch (IllegalAccessException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//            entity.deleteLast();
//            entityList.add(entity);
//        }
//        return entityList;
//    }
//
//    public void setInsert(Entity entity, SetData setData, List<?> targets) {
//        entity.append("set ");
//        Class<?> target = targets.get(0).getClass();
//        Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(target);
//        IExpression expression = setData.getExpression();
//        if (expression instanceof BlockExpression) {
//            BlockExpression blockExpression = (BlockExpression) expression;
//            for (IExpression iExpression : blockExpression.getExpressions()) {
//                if (iExpression instanceof MethodCallExpression) {
//                    MethodCallExpression methodCallExpression = (MethodCallExpression) iExpression;
//                    String fieldName = GetSetHelper.getterToFieldName(methodCallExpression.getSelectedMethod(), target);
//                    entity.append(map.get(fieldName)).append(" = ");
//                    IExpression val = methodCallExpression.getParams().get(0);
//                    doResolve(entity, val, targets);
//                }
//                entity.append(",");
//            }
//            entity.deleteLast().append(" ");
//        } else if (expression instanceof MethodCallExpression) {
//            MethodCallExpression methodCallExpression = (MethodCallExpression) expression;
//            String fieldName = GetSetHelper.getterToFieldName(methodCallExpression.getSelectedMethod(), target);
//            entity.append(map.get(fieldName)).append(" = ");
//            IExpression val = methodCallExpression.getParams().get(0);
//            doResolve(entity, val, targets);
//        } else {
//            throw new RuntimeException(expression.toString());
//        }
//    }
//
//    public void setUpdate(Entity entity, SetData setData, List<?> targets) {
//        entity.append("set ");
//        Class<?> target = targets.get(0).getClass();
//        Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(target);
//        IExpression expression = setData.getExpression();
//        if (expression instanceof BlockExpression) {
//            BlockExpression blockExpression = (BlockExpression) expression;
//            for (IExpression iExpression : blockExpression.getExpressions()) {
//                doResolve(entity, iExpression, targets);
//                entity.append(",");
//            }
//            entity.deleteLast().append(" ");
//        } else if (expression instanceof MethodCallExpression) {
//            doResolve(entity, expression, targets);
//        } else if (expression instanceof AssignExpression) {
//            doResolve(entity, expression, targets);
//        } else {
//            throw new RuntimeException(expression.toString());
//        }
//    }
//
//    public void join(Entity entity, Join join, List<Class<?>> queryClass) {
//        switch (join.getJoinType()) {
//            case Inner:
//                entity.append("inner join ");
//                break;
//            case Left:
//                entity.append("left join ");
//                break;
//            case Right:
//                entity.append("right join ");
//                break;
//            case Full:
//                entity.append("full join ");
//                break;
//        }
//        entity.append(ClassDefinitionCache.getTableName(join.getJoinClass()))
//                .append(" ").append("as").append(" ")
//                .append(Resolve.alias(queryClass.indexOf(join.getJoinClass()))).append(" ");
//    }
//
//    public void select(Entity entity, boolean isDistinct, NewExpression<?> newExpression, List<Class<?>> queryClass, List<?> queryTarget, List<Class<?>> joinClass) {
//        entity.append("select").append(" ");
//        if (isDistinct) entity.append("distinct").append(" ");
//        if (!newExpression.getExpressions().isEmpty()) {
//            doResolve(entity, newExpression, queryTarget);
//        } else {
//            entity.append(Resolve.alias(queryClass.indexOf(newExpression.getTarget()))).append(".").append("*");
//        }
//        entity.append(" ");
//        entity.append("from").append(" ");
//        for (Class<?> c : queryClass) {
//            if (joinClass == null || !joinClass.contains(c)) {
//                entity.append(ClassDefinitionCache.getTableName(c)).append(" ").append("as").append(" ")
//                        .append(Resolve.alias(queryClass.indexOf(c))).append(",");
//            }
//        }
//        entity.deleteLast().append(" ");
//    }
//
//    public void where(Entity entity, IExpression expression, List<?> queryTarget) {
//        entity.append("where").append(" ");
//        doResolve(entity, expression, queryTarget);
//    }
//
//    public void on(Entity entity, IExpression expression, List<?> queryTarget) {
//        entity.append("on").append(" ");
//        doResolve(entity, expression, queryTarget);
//    }
//
//    public void take(Entity entity, Take take) {
//        entity.append("limit").append(" ");
//        entity.append(take.getCount()).append(" ");
//    }
//
//    public void skip(Entity entity, Skip skip) {
//        entity.append("offset").append(" ");
//        entity.append(skip.getCount()).append(" ");
//    }
//
//    public void orderBy(Entity entity, OrderBy orderBy, List<?> queryTarget) {
//        IExpression ref = orderBy.getExpression();
//        entity.append("order by ");
//        doResolve(entity, ref, queryTarget);
//        entity.blank();
//    }
//
//    public void doResolveAssignExpression(AssignExpression assign, Entity entity, List<?> queryTarget) {
//        doResolve(entity, assign.getLeft(), queryTarget);
//        entity.append(" = ");
//        doResolve(entity, assign.getRight(), queryTarget);
//        entity.blank();
//    }
//
//    public void doResolveNewExpression(NewExpression<?> newExpression, Entity entity, List<?> queryTarget) {
//        for (IExpression expression : newExpression.getExpressions()) {
//            if (expression instanceof MethodCallExpression) {
//                MethodCallExpression methodCallExpression = (MethodCallExpression) expression;
//                for (IExpression param : methodCallExpression.getParams()) {
//                    doResolve(entity, param, queryTarget);
//                }
//                entity.append(",");
//            }
//        }
//        entity.deleteLast();
//    }
//
//    public void doResolveReferenceExpression(ReferenceExpression reference, Entity entity, List<?> queryTarget) {
//        Object ref = reference.getReference();
//        if (queryTarget.contains(ref)) {
//            int index = queryTarget.indexOf(ref);
//            entity.append(Resolve.alias(index));
//        } else {
//            entity.append("?");
//            entity.values.add(ref);
//        }
//    }
//
//    public void doResolveFieldSelectExpression(FieldSelectExpression fieldSelect, Entity entity, List<?> queryTarget) {
//        Object reference = fieldSelect.getSource().getReference();
//        if (queryTarget.contains(reference)) {
//            doResolve(entity, fieldSelect.getSelector(), queryTarget);
//            entity.append(".");
//            Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(reference.getClass());
//            entity.append(map.get(fieldSelect.getSelectedField()));
//        } else {
//            entity.append("?").append(" ");
//            Object val = fieldSelect.getValue();
//            entity.values.add(val);
//        }
//    }
//
//    public void doResolveMethodCallExpression(MethodCallExpression methodCall, Entity entity, List<?> queryTarget) {
//        Object reference = methodCall.getSource().getReference();
//        if (queryTarget.contains(reference)) {
//            switch (methodCall.getSelectedMethod()) {
//                case "equals":
//                    doResolve(entity, methodCall.getSelector(), queryTarget);
//                    entity.append(" = ");
//                    doResolve(entity, methodCall.getParams().get(0), queryTarget);
//                    break;
//                case "contains":
//                    doResolve(entity, methodCall.getSelector(), queryTarget);
//                    entity.append(" like ");
//                    doResolve(entity, methodCall.getParams().get(0), queryTarget);
//                    int index = entity.values.size() - 1;
//                    entity.values.set(index, "%" + entity.values.get(index) + "%");
//                    break;
//                default:
//                    doResolve(entity, methodCall.getSelector(), queryTarget);
//                    Map<String, String> map = ClassDefinitionCache.getJavaFieldNameToDbFieldNameMappingMap(reference.getClass());
//                    entity.append(".").append(map.get(GetSetHelper.getterToFieldName(
//                            methodCall.getSelectedMethod(),
//                            reference.getClass())
//                    ));
//                    if (!methodCall.getParams().isEmpty()) {
//                        entity.append(" = ");
//                        doResolve(entity, methodCall.getParams().get(0), queryTarget);
//                    }
//                    break;
//            }
//        } else {
//            entity.append("?").append(" ");
//            Object val = methodCall.getValue();
//            entity.values.add(val);
//        }
//    }
//
//    public void doResolveMappingExpression(MappingExpression mapping, Entity entity, List<?> queryTarget) {
//        doResolve(entity, mapping.getValue(), queryTarget);
//    }
//
//    public void doResolveUnaryExpression(UnaryExpression unary, Entity entity, List<?> queryTarget) {
//        if (unary.getOperator() == Operator.NOT) {
//            if (unary.getExpression() instanceof ParensExpression) {
//                entity.append("!");
//                doResolve(entity, unary.getExpression(), queryTarget);
//            } else {
//                entity.append("!(");
//                doResolve(entity, unary.getExpression(), queryTarget);
//                entity.append(")").append(" ");
//            }
//        }
//    }
//
//    public void doResolveValueExpression(ValueExpression<?> value, Entity entity) {
//        if (value.getValue() != null) {
//            entity.append("?");
//            entity.values.add(value.getValue());
//        } else {
//            entity.append("null");
//        }
//    }
//
//    public void doResolveBinaryExpression(BinaryExpression binary, Entity entity, List<?> queryTarget) {
//        doResolve(entity, binary.getLeft(), queryTarget);
//        entity.blank();
//        if (binary.getRight() instanceof ValueExpression && ((ValueExpression<?>) binary.getRight()).getValue() == null) {
//            switch (binary.getOperator()) {
//                case EQ:
//                    entity.append("is");
//                    break;
//                case NE:
//                    entity.append("is not");
//                    break;
//            }
//        } else {
//            entity.append(binary.getOperator().toString());
//        }
//        entity.blank();
//        doResolve(entity, binary.getRight(), queryTarget);
//        entity.blank();
//    }
//
//    public void doResolveParensExpression(ParensExpression parens, Entity entity, List<?> queryTarget) {
//        entity.append("(");
//        doResolve(entity, parens.getExpression(), queryTarget);
//        entity.deleteLast().append(")").append(" ");
//    }
//
//    @Override
//    public Type type() {
//        return Type.Mysql;
//    }
//
//
//}
