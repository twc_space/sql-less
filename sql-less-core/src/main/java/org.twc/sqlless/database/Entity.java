package org.twc.sqlless.database;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Entity {

    private static final ConcurrentLinkedDeque<Entity> Pool = new ConcurrentLinkedDeque<>() {
        {
            for (int i = 0; i < 128; i++) {//初始化并且复用
                Pool.push(new Entity());
            }
        }
    };


    static Entity create() {
        Entity res = Pool.poll();
        if (res == null) {
            res = new Entity();
        }
        return res;
    }

    static void release(Entity entity) {
        entity.clear();
        if (Pool.size() > 256) return;
        Pool.push(entity);
    }

    static void release(Entity... entity) {
        for (Entity e : entity) {
            release(e);
        }
    }

    static void release(List<Entity> entity) {
        for (Entity e : entity) {
            release(e);
        }
    }

    /************************************************************************************************************/


    public final StringBuilder sql = new StringBuilder();

    public final List<Object> values = new ArrayList<>();

    private Entity() {}

    public String toSql() {
        return sql.toString();
    }

    <T> Entity append(T t) {
        sql.append(t);
        return this;
    }

    Entity append(String s) {
        sql.append(s);
        return this;
    }

    Entity blank() {
        if (sql.charAt(sql.length() - 1) != ' ') append(" ");
        return this;
    }

    Entity deleteLast() {
        if (sql.length() > 0) {
            sql.deleteCharAt(sql.length() - 1);
        }
        return this;
    }

    Entity tryDeleteLast() {
        if (sql.length() > 0) {
            int last = sql.length() - 1;
            switch (sql.charAt(last)) {
                case ',':
                case ' ':
                case '(':
                case ')':
                    sql.deleteCharAt(last);
                    tryDeleteLast();
                    break;
            }
        }
        return this;
    }

    Entity pushValue(Object o) {
        values.add(o);
        return this;
    }

    void clear() {
        sql.setLength(0);
        values.clear();
    }

    @Override
    public String toString() {
        return "Sql: " + sql.toString() + "\n" +
                "values: " + values.toString();
    }

}
