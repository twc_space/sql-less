//package org.twc.sqlless.database;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.core.statement.Statement;
//import org.twc.sqlless.expression.*;
//
//import java.util.List;
//import java.util.Objects;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ConcurrentMap;
//
///**
// * 解析器管理
// *
// * @author twc
// * @date 2023/11/24
// */
//public final class ResolveManager implements Resolve<SqlClient<?>> {
//    //TODO 去掉
//    private final static ThreadLocal<String/*name*/> current = ThreadLocal.withInitial(() -> null);
//
//    private volatile String defaultName;
//
//    private final ConcurrentMap<String/*name*/, Resolve<SqlClient<?>>> delegation = new ConcurrentHashMap<>();
//
//    public static void current(String name) {
//        current.set(name);
//    }
//
//    public ResolveManager setDefault(String defaultName) {
//        this.defaultName = defaultName;
//        return this;
//    }
//
//    public ResolveManager register(String resolveName, Resolve<SqlClient<?>> resolve) {
//        delegation.putIfAbsent(resolveName, resolve);
//        return this;
//    }
//
//    public ResolveManager unregister(String resolveName) {
//        delegation.remove(resolveName);
//        return this;
//    }
//
//    @Override
//    public SqlClient<SqlClient<?>> getClient() {
//        throw new UnsupportedOperationException("ResolveManager 不支持该方法");
//    }
//
//    @Override
//    public Entity query(boolean isDistinct, List<Base> bases, NewExpression<?> newExpression, List<Class<?>> queryClasses, List<?> queryTarget, List<Class<?>> joins) {
//        return this.find().query(isDistinct, bases, newExpression, queryClasses, queryTarget, joins);
//    }
//
//    @Override
//    public Entity cud(Statement<?> statement) {
//        return this.find().cud(statement);
//    }
//
//    @Override
//    public Entity insert(Statement<?> statement) {
//        return this.find().insert(statement);
//    }
//
//    @Override
//    public Entity delete(Statement<?> statement) {
//        return this.find().delete(statement);
//    }
//
//    @Override
//    public Entity update(Statement<?> statement) {
//        return this.find().update(statement);
//    }
//
//    @Override
//    public <T> List<Entity> batchSave(List<T> ts) {
//        return this.find().batchSave(ts);
//    }
//
//    @Override
//    public void setInsert(Entity entity, SetData setData, List<?> targets) {
//        this.find().setInsert(entity, setData, targets);
//    }
//
//    @Override
//    public void setUpdate(Entity entity, SetData setData, List<?> targets) {
//        this.find().setUpdate(entity, setData, targets);
//    }
//
//    @Override
//    public void join(Entity entity, Join join, List<Class<?>> queryClass) {
//        this.find().join(entity, join, queryClass);
//    }
//
//    @Override
//    public void select(Entity entity, boolean isDistinct, NewExpression<?> newExpression, List<Class<?>> queryClass, List<?> queryTarget, List<Class<?>> joinClass) {
//        this.find().select(entity, isDistinct, newExpression, queryClass, queryTarget, joinClass);
//    }
//
//    @Override
//    public void where(Entity entity, IExpression expression, List<?> queryTarget) {
//        this.find().where(entity, expression, queryTarget);
//    }
//
//    @Override
//    public void on(Entity entity, IExpression expression, List<?> queryTarget) {
//        this.find().on(entity, expression, queryTarget);
//    }
//
//    @Override
//    public <T> Entity save(T save) {
//        return this.find().save(save);
//    }
//
//
//    @Override
//    public void take(Entity entity, Take take) {
//        this.find().take(entity, take);
//    }
//
//    @Override
//    public void skip(Entity entity, Skip skip) {
//        this.find().skip(entity, skip);
//    }
//
//    @Override
//    public void orderBy(Entity entity, OrderBy orderBy, List<?> queryTarget) {
//        this.find().orderBy(entity, orderBy, queryTarget);
//    }
//
//    @Override
//    public void doResolveAssignExpression(AssignExpression assign, Entity entity, List<?> queryTarget) {
//        this.find().doResolveAssignExpression(assign, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveNewExpression(NewExpression<?> newExpression, Entity entity, List<?> queryTarget) {
//        this.find().doResolveNewExpression(newExpression, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveReferenceExpression(ReferenceExpression reference, Entity entity, List<?> queryTarget) {
//        this.find().doResolveReferenceExpression(reference, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveFieldSelectExpression(FieldSelectExpression fieldSelect, Entity entity, List<?> queryTarget) {
//        this.find().doResolveFieldSelectExpression(fieldSelect, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveMethodCallExpression(MethodCallExpression methodCall, Entity entity, List<?> queryTarget) {
//        this.find().doResolveMethodCallExpression(methodCall, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveMappingExpression(MappingExpression mapping, Entity entity, List<?> queryTarget) {
//        this.find().doResolveMappingExpression(mapping, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveUnaryExpression(UnaryExpression unary, Entity entity, List<?> queryTarget) {
//        this.find().doResolveUnaryExpression(unary, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveValueExpression(ValueExpression<?> value, Entity entity) {
//        this.find().doResolveValueExpression(value, entity);
//    }
//
//    @Override
//    public void doResolveBinaryExpression(BinaryExpression binary, Entity entity, List<?> queryTarget) {
//        this.find().doResolveBinaryExpression(binary, entity, queryTarget);
//    }
//
//    @Override
//    public void doResolveParensExpression(ParensExpression parens, Entity entity, List<?> queryTarget) {
//        this.find().doResolveParensExpression(parens, entity, queryTarget);
//    }
//
//    private Resolve<SqlClient<?>> find() {
//        try {
//            String name = current.get();
//            Resolve<SqlClient<?>> resolve = null;
//            if (Objects.nonNull(name)) {
//                resolve = delegation.get(name);
//            } else if (Objects.nonNull(defaultName)) {
//                resolve = delegation.get(defaultName);
//            }
//            if (Objects.isNull(resolve)) {
//                throw new IllegalStateException("未能找到SQL解析器:" + (Objects.nonNull(name) ? name : defaultName));
//            }
//            return resolve;
//        } finally {
//            current.remove();
//        }
//    }
//
//}
