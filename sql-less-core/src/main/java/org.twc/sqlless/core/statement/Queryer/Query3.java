//package org.twc.sqlless.core.statement.Queryer;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.func.Func000;
//import org.twc.sqlless.core.func.Func100;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.core.statement.Statement3;
//
//import java.util.List;
//
//public class Query3<T1, T2, T3> extends Statement3<T1, T2, T3> {
//
//    public Query3(List<Base> bases, List<Class<?>> joins, Class<T1> c1, Class<T2> c2, Class<T3> c3, Resolve<SqlClient<?>> resolve) {
//        super(bases, joins, c1, c2, c3, resolve);
//    }
//
//    public Query3<T1, T2, T3> on(Func100<T1, T2, T3> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query3<T1, T2, T3> where(Func100<T1, T2, T3> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query3<T1, T2, T3> orderBy(Func000<T1, T2, T3, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query3<T1, T2, T3> descOrderBy(Func000<T1, T2, T3, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> org.twc.sqlless.core.statement.Queryer.QueryResult<R> select(Func000<T1, T2, T3, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query3<T1, T2, T3> on(ExpressionFunc.E3<Void, T1, T2, T3> e3) {
//        bases.add(new On(e3.invoke(null, t1, t2, t3)));
//        return this;
//    }
//
//
//    public Query3<T1, T2, T3> where(ExpressionFunc.E3<Void, T1, T2, T3> e3) {
//        bases.add(new Where(e3.invoke(null, t1, t2, t3)));
//        return this;
//    }
//
//    public <R> org.twc.sqlless.core.statement.Queryer.QueryResult<R> select(ExpressionFunc.NR3<Void, T1, T2, T3, R> r) {
//        return new QueryResult<R>(bases, getQueryClasses(), getQueryTargets(), joins, r.invoke(null, t1, t2, t3), resolve);
//    }
//
//    public Query3<T1, T2, T3> orderBy(ExpressionFunc.E3<Void, T1, T2, T3> e3) {
//        bases.add(new OrderBy(e3.invoke(null, t1, t2, t3), false));
//        return this;
//    }
//
//    public Query3<T1, T2, T3> descOrderBy(ExpressionFunc.E3<Void, T1, T2, T3> e3) {
//        bases.add(new OrderBy(e3.invoke(null, t1, t2, t3), true));
//        return this;
//    }
//
//    public Query3<T1, T2, T3> take(int count) {
//        bases.add(new Take(count));
//        return this;
//    }
//
//    public Query3<T1, T2, T3> skip(int count) {
//        bases.add(new Skip(count));
//        return this;
//    }
//
//    public <T4> Query4<T1, T2, T3, T4> innerJoin(Class<T4> c) {
//        bases.add(new Join(c, Join.Type.Inner));
//        joins.add(c);
//        return new Query4<>(bases, joins, c1, c2, c3, c, resolve);
//    }
//
//    public <T4> Query4<T1, T2, T3, T4> leftJoin(Class<T4> c) {
//        bases.add(new Join(c, Join.Type.Left));
//        joins.add(c);
//        return new Query4<>(bases, joins, c1, c2, c3, c, resolve);
//    }
//
//    public <T4> Query4<T1, T2, T3, T4> rightJoin(Class<T4> c) {
//        bases.add(new Join(c, Join.Type.Right));
//        joins.add(c);
//        return new Query4<>(bases, joins, c1, c2, c3, c, resolve);
//    }
//
//    public <T4> Query4<T1, T2, T3, T4> fullJoin(Class<T4> c) {
//        bases.add(new Join(c, Join.Type.Full));
//        joins.add(c);
//        return new Query4<>(bases, joins, c1, c2, c3, c, resolve);
//    }
//
//}
