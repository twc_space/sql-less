//package org.twc.sqlless.core.statement.Queryer;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.func.Func00;
//import org.twc.sqlless.core.func.Func10;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.core.statement.Statement2;
//
//import java.util.List;
//
//public class Query2<T1, T2> extends Statement2<T1, T2> {
//
//    public Query2(List<Base> bases, List<Class<?>> joins, Class<T1> c1, Class<T2> c2, Resolve<SqlClient<?>> resolve) {
//        super(bases, joins, c1, c2, resolve);
//    }
//
//    public Query2<T1, T2> on(Func10<T1, T2> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query2<T1, T2> where(Func10<T1, T2> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query2<T1, T2> orderBy(Func00<T1, T2, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query2<T1, T2> descOrderBy(Func00<T1, T2, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> QueryResult<R> select(Func00<T1, T2, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query2<T1, T2> on(ExpressionFunc.E2<Void, T1, T2> e2) {
//        bases.add(new On(e2.invoke(null, t1, t2)));
//        return this;
//    }
//
//    public Query2<T1, T2> where(ExpressionFunc.E2<Void, T1, T2> e2) {
//        bases.add(new Where(e2.invoke(null, t1, t2)));
//        return this;
//    }
//
//    public Query2<T1, T2> orderBy(ExpressionFunc.E2<Void, T1, T2> e2) {
//        bases.add(new OrderBy(e2.invoke(null, t1, t2), false));
//        return this;
//    }
//
//    public Query2<T1, T2> descOrderBy(ExpressionFunc.E2<Void, T1, T2> e2) {
//        bases.add(new OrderBy(e2.invoke(null, t1, t2), true));
//        return this;
//    }
//
//    public <R> QueryResult<R> select(ExpressionFunc.NR2<Void, T1, T2, R> r) {
//        return new QueryResult<R>(bases, getQueryClasses(), getQueryTargets(), joins, r.invoke(null, t1, t2), resolve);
//    }
//
//    public Query2<T1, T2> take(int count) {
//        bases.add(new Take(count));
//        return this;
//    }
//
//    public Query2<T1, T2> skip(int count) {
//        bases.add(new Skip(count));
//        return this;
//    }
//
//    public <T3> Query3<T1, T2, T3> innerJoin(Class<T3> c) {
//        bases.add(new Join(c, Join.Type.Inner));
//        joins.add(c);
//        return new Query3<>(bases, joins, c1, c2, c,resolve);
//    }
//
//    public <T3> Query3<T1, T2, T3> leftJoin(Class<T3> c) {
//        bases.add(new Join(c, Join.Type.Left));
//        joins.add(c);
//        return new Query3<>(bases, joins, c1, c2, c,resolve);
//    }
//
//    public <T3> Query3<T1, T2, T3> rightJoin(Class<T3> c) {
//        bases.add(new Join(c, Join.Type.Right));
//        joins.add(c);
//        return new Query3<>(bases, joins, c1, c2, c,resolve);
//    }
//
//    public <T3> Query3<T1, T2, T3> fullJoin(Class<T3> c) {
//        bases.add(new Join(c, Join.Type.Full));
//        joins.add(c);
//        return new Query3<>(bases, joins, c1, c2, c,resolve);
//    }
//
//}
