//package org.twc.sqlless.core.statement;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.Base;
//import org.twc.sqlless.core.Resolve;
//
//import java.lang.reflect.InvocationTargetException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 单表
// *
// * @author twc
// * @date 2023/11/25
// */
//public abstract class Statement<T> {
//    protected final T t1;//映射类
//    protected final Class<T> c1;//映射类.class
//    protected final List<Base> bases = new ArrayList<>();
//    protected final Resolve<SqlClient<?>> resolve;
//
//    public Statement(Class<T> c1, Resolve<SqlClient<?>> resolve) {
//        this.c1 = c1;
//        this.resolve = resolve;
//        try {
//            this.t1 = c1.getConstructor().newInstance();
//        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
//                NoSuchMethodException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public List<Base> getBases() {
//        return bases;
//    }
//
//    public List<Class<?>> getQueryClasses() {
//        List<Class<?>> list = new ArrayList<>();
//        list.add(c1);
//        return list;
//    }
//
//    public List<?> getQueryTargets() {
//        List<Object> list = new ArrayList<>();
//        list.add(t1);
//        return list;
//    }
//
//    public Resolve<SqlClient<?>> getResolve() {
//        return resolve;
//    }
//
//}
