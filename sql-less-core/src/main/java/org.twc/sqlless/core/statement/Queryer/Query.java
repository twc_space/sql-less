//package org.twc.sqlless.core.statement.Queryer;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.*;
//import org.twc.sqlless.core.func.Func0;
//import org.twc.sqlless.core.func.Func1;
//import org.twc.sqlless.core.func.Func2;
//import org.twc.sqlless.core.statement.Statement;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.expression.IExpression;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * 单表查询
// *
// * @author twc
// * @date 2023/11/25
// */
//public class Query<T> extends Statement<T> {
//
//    private Entity entity = null;
//
//    public Query(Class<T> c1, Resolve<SqlClient<?>> resolve) {
//        super(c1, resolve);
//    }
//
//    public String toSql() {
//        tryGetEntity();
//        return entity.toSql();
//    }
//
//    public Entity toEntity() {
//        tryGetEntity();
//        return entity;
//    }
//
//    public void toEntityAndThen(Func2<Entity> then) {
//        tryGetEntity();
//        then.invoke(entity);
//    }
//
//    public List<T> toList() {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, IExpression.New(c1));
//    }
//
//    public <Key> Map<Key, T> toMap(Func0<T, Key> getKey) {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, IExpression.New(c1), getKey);
//    }
//
//    public <Key, Value> Map<Key, Value> toMap(Func0<T, Key> getKey, Func0<T, Value> getValue) {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, IExpression.New(c1), getKey, getValue);
//    }
//
//    public void toListAndThen(Func2<List<T>> then) {
//        tryGetEntity();
//        List<T> list = resolve.getClient().startQuery(entity, IExpression.New(c1));
//        then.invoke(list);
//    }
//
//    public <Key> void toMapAndThen(Func0<T, Key> getKey, Func2<Map<Key, T>> then) {
//        tryGetEntity();
//        Map<Key, T> map = resolve.getClient().startQuery(entity, IExpression.New(c1), getKey);
//        then.invoke(map);
//    }
//
//    public <Key, Value> void toMapAndThen(Func0<T, Key> getKey, Func0<T, Value> getValue, Func2<Map<Key, Value>> then) {
//        tryGetEntity();
//        Map<Key, Value> map = resolve.getClient().startQuery(entity, IExpression.New(c1), getKey, getValue);
//        then.invoke(map);
//    }
//
//    private void tryGetEntity() {
//        if (entity == null) {
//            entity = resolve.query(false, bases, IExpression.New(c1), getQueryClasses(), getQueryTargets(), null);
//        }
//    }
//
//    public Query<T> where(Func1<T> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query<T> orderBy(Func0<T, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query<T> descOrderBy(Func0<T, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> QueryResult<R> select(Func0<T, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query<T> where(ExpressionFunc.E1<Void, T> e1) {
//        bases.add(new Where(e1.invoke(null, t1)));
//        return this;
//    }
//
//    public <R> QueryResult<R> select(ExpressionFunc.NR1<Void, T, R> r) {
//        return new QueryResult<R>(bases, getQueryClasses(), getQueryTargets(), null, r.invoke(null, t1), resolve);
//    }
//
//    public Query<T> orderBy(ExpressionFunc.E1<Void, T> e1) {
//        bases.add(new OrderBy(e1.invoke(null, t1), false));
//        return this;
//    }
//
//    public Query<T> descOrderBy(ExpressionFunc.E1<Void, T> e1) {
//        bases.add(new OrderBy(e1.invoke(null, t1), true));
//        return this;
//    }
//
//    public Query<T> take(int count) {
//        bases.add(new Take(count));
//        return this;
//    }
//
//    public Query<T> skip(int count) {
//        bases.add(new Skip(count));
//        return this;
//    }
//
//    public <T2> Query2<T, T2> innerJoin(Class<T2> c) {
//        bases.add(new Join(c, Join.Type.Inner));
//        List<Class<?>> list = new ArrayList<>();
//        list.add(c);
//        return new Query2<>(bases, list, c1, c,resolve);
//    }
//
//    public <T2> Query2<T, T2> leftJoin(Class<T2> c) {
//        bases.add(new Join(c, Join.Type.Left));
//        List<Class<?>> list = new ArrayList<>();
//        list.add(c);
//        return new Query2<>(bases, list, c1, c,resolve);
//    }
//
//    public <T2> Query2<T, T2> rightJoin(Class<T2> c) {
//        bases.add(new Join(c, Join.Type.Right));
//        List<Class<?>> list = new ArrayList<>();
//        list.add(c);
//        return new Query2<>(bases, list, c1, c,resolve);
//    }
//
//    public <T2> Query2<T, T2> fullJoin(Class<T2> c) {
//        bases.add(new Join(c, Join.Type.Full));
//        List<Class<?>> list = new ArrayList<>();
//        list.add(c);
//        return new Query2<>(bases, list, c1, c,resolve);
//    }
//}
