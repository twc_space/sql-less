//package org.twc.sqlless.core.statement.Inserter;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.SetData;
//import org.twc.sqlless.core.statement.CUD;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.core.func.Func2;
//import org.twc.sqlless.core.Resolve;
//
///**
// * 新增
// *
// * @author twc
// * @date 2023/11/25
// */
//public class Insert<T> extends CUD<T> {
//
//    private Entity entity = null;
//
//    public Insert(Class<T> c1, Resolve<SqlClient<?>> resolve) {
//        super(c1, resolve);
//    }
//
//    public Insert<T> set(Func2<T> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Insert<T> set(ExpressionFunc.E1<Void, T> e1) {
//        bases.add(new SetData(e1.invoke(null, t1)));
//        return this;
//    }
//
//    public Entity toEntity() {
//        return resolve.cud(this);
//    }
//
//    public String toSql() {
//        tryGetEntity();
//        return entity.toSql();
//    }
//
//    public int doSave() {
//        tryGetEntity();
//        return resolve.getClient().startUpdate(entity);
//    }
//
//    private void tryGetEntity() {
//        if (entity == null) {
//            entity = resolve.insert(this);
//        }
//    }
//
//}
