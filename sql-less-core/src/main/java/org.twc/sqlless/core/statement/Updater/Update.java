//package org.twc.sqlless.core.statement.Updater;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.SetData;
//import org.twc.sqlless.core.base.Where;
//import org.twc.sqlless.core.statement.CUD;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.core.func.Func1;
//import org.twc.sqlless.core.func.Func2;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.expression.IExpression;
//
///**
// * 更新
// *
// * @author twc
// * @date 2023/11/25
// */
//public class Update<T> extends CUD<T> {
//
//    private Entity entity = null;
//
//    public Update(Class<T> c1, Resolve<SqlClient<?>> resolve) {
//        super(c1, resolve);
//    }
//
//    public Update<T> set(Func2<T> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Update<T> set(ExpressionFunc.E1<Void, T> e1) {
//        bases.add(new SetData(e1.invoke(null, t1)));
//        return this;
//    }
//
//    public Update<T> where(Func1<T> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Update<T> where(IExpression expression) {
//        bases.add(new Where(expression));
//        return this;
//    }
//
//    public int doUpdate() {
//        tryGetEntity();
//        return resolve.getClient().startUpdate(entity);
//    }
//
//    public Entity toEntity() {
//        tryGetEntity();
//        return entity;
//    }
//
//    public String toSql() {
//        tryGetEntity();
//        return entity.toSql();
//    }
//
//    private void tryGetEntity() {
//        if (entity == null) {
//            entity = resolve.update(this);
//        }
//    }
//}
