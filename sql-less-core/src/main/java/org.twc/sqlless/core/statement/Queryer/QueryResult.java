//package org.twc.sqlless.core.statement.Queryer;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.Base;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.core.func.Func0;
//import org.twc.sqlless.core.func.Func2;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.expression.NewExpression;
//
//import java.util.List;
//import java.util.Map;
//
//public class QueryResult<R> {
//    private final List<Base> bases;
//    private final List<Class<?>> queryClasses;
//    private final List<?> queryTargets;
//    private final List<Class<?>> joinClasses;
//    private final NewExpression<R> newExpression;
//    private boolean isDistinct;
//    private Entity entity = null;
//    private final Resolve<SqlClient<?>> resolve;
//
//    public QueryResult(List<Base> bases, List<Class<?>> queryClasses, List<?> queryTargets, List<Class<?>> joinClasses, NewExpression<R> newExpression, Resolve<SqlClient<?>> resolve) {
//        this.queryClasses = queryClasses;
//        this.queryTargets = queryTargets;
//        this.joinClasses = joinClasses;
//        this.bases = bases;
//        this.newExpression = newExpression;
//        this.resolve = resolve;
//    }
//
//    public String toSql() {
//        tryGetEntity();
//        return entity.sql.toString();
//    }
//
//    public Entity toEntity() {
//        tryGetEntity();
//        return entity;
//    }
//
//    public void toEntityAndThen(Func2<Entity> then) {
//        tryGetEntity();
//        then.invoke(entity);
//    }
//
//    public List<R> toList() {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, newExpression);
//    }
//
//    public <Key> Map<Key, R> toMap(Func0<R, Key> getKey) {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, newExpression, getKey);
//    }
//
//    public <Key, Value> Map<Key, Value> toMap(Func0<R, Key> getKey, Func0<R, Value> getValue) {
//        tryGetEntity();
//        return resolve.getClient().startQuery(entity, newExpression, getKey, getValue);
//    }
//
//    public void toListAndThen(Func2<List<R>> then) {
//        tryGetEntity();
//        List<R> list = resolve.getClient().startQuery(entity, newExpression);
//        then.invoke(list);
//    }
//
//    public <Key> void toMapAndThen(Func0<R, Key> getKey, Func2<Map<Key, R>> then) {
//        tryGetEntity();
//        Map<Key, R> map = resolve.getClient().startQuery(entity, newExpression, getKey);
//        then.invoke(map);
//    }
//
//    public <Key, Value> void toMapAndThen(Func0<R, Key> getKey, Func0<R, Value> getValue, Func2<Map<Key, Value>> then) {
//        tryGetEntity();
//        Map<Key, Value> map = resolve.getClient().startQuery(entity, newExpression, getKey, getValue);
//        then.invoke(map);
//    }
//
//    public QueryResult<R> distinct() {
//        return distinct(true);
//    }
//
//    public QueryResult<R> distinct(boolean sw) {
//        isDistinct = sw;
//        return this;
//    }
//
//    public boolean isDistinct() {
//        return isDistinct;
//    }
//
//    public NewExpression<R> getNewExpression() {
//        return newExpression;
//    }
//
//    private void tryGetEntity() {
//        if (entity == null) {
//            entity = resolve.query(isDistinct, bases, newExpression, queryClasses, queryTargets, joinClasses);
//        }
//    }
//}
