//package org.twc.sqlless.core.statement.Queryer;
//
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.func.Func0000;
//import org.twc.sqlless.core.func.Func1000;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.core.statement.Statement4;
//
//import java.util.List;
//
//public class Query4<T1, T2, T3, T4> extends Statement4<T1, T2, T3, T4> {
//
//    public Query4(List<Base> bases, List<Class<?>> joins, Class<T1> c1, Class<T2> c2, Class<T3> c3, Class<T4> c4, Resolve<SqlClient<?>> resolve) {
//        super(bases, joins, c1, c2, c3, c4, resolve);
//    }
//
//    public Query4<T1, T2, T3, T4> on(Func1000<T1, T2, T3, T4> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query4<T1, T2, T3, T4> where(Func1000<T1, T2, T3, T4> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query4<T1, T2, T3, T4> orderBy(Func0000<T1, T2, T3, T4, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> Query4<T1, T2, T3, T4> descOrderBy(Func0000<T1, T2, T3, T4, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public <R> QueryResult<R> select(Func0000<T1, T2, T3, T4, R> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Query4<T1, T2, T3, T4> on(ExpressionFunc.E4<Void, T1, T2, T3, T4> e4) {
//        bases.add(new On(e4.invoke(null, t1, t2, t3, t4)));
//        return this;
//    }
//
//
//    public Query4<T1, T2, T3, T4> where(ExpressionFunc.E4<Void, T1, T2, T3, T4> e4) {
//        bases.add(new Where(e4.invoke(null, t1, t2, t3, t4)));
//        return this;
//    }
//
//    public <R> QueryResult<R> select(ExpressionFunc.NR4<Void, T1, T2, T3, T4, R> r) {
//        return new QueryResult<R>(bases, getQueryClasses(), getQueryTargets(), joins, r.invoke(null, t1, t2, t3, t4), resolve);
//    }
//
//
//    public Query4<T1, T2, T3, T4> orderBy(ExpressionFunc.E4<Void, T1, T2, T3, T4> e4) {
//        bases.add(new OrderBy(e4.invoke(null, t1, t2, t3, t4), false));
//        return this;
//    }
//
//    public Query4<T1, T2, T3, T4> descOrderBy(ExpressionFunc.E4<Void, T1, T2, T3, T4> e4) {
//        bases.add(new OrderBy(e4.invoke(null, t1, t2, t3, t4), true));
//        return this;
//    }
//
//    public Query4<T1, T2, T3, T4> take(int count) {
//        bases.add(new Take(count));
//        return this;
//    }
//
//    public Query4<T1, T2, T3, T4> skip(int count) {
//        bases.add(new Skip(count));
//        return this;
//    }
//}
