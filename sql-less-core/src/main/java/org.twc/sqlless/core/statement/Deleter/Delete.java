//package org.twc.sqlless.core.statement.Deleter;
//
//import org.twc.sqlless.core.SqlClient;
//import org.twc.sqlless.core.base.Where;
//import org.twc.sqlless.core.statement.CUD;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.core.func.Func1;
//import org.twc.sqlless.core.Resolve;
//import org.twc.sqlless.expression.IExpression;
//
///**
// * 删除
// *
// * @author twc
// * @date 2023/11/25
// */
//public class Delete<T> extends CUD<T> {
//    private Entity entity = null;
//
//    public Delete(Class<T> c1, Resolve<SqlClient<?>> resolve) {
//        super(c1, resolve);
//    }
//
//    public Delete<T> where(Func1<T> func) {
//        throw new IllegalStateException("编译未能生成实现代码，请尝试重新编译");
//    }
//
//    public Delete<T> where(IExpression expression) {
//        bases.add(new Where(expression));
//        return this;
//    }
//
//    public Entity toEntity() {
//        tryGetEntity();
//        return entity;
//    }
//
//    public String toSql() {
//        tryGetEntity();
//        return entity.toSql();
//    }
//
//    public int doDelete() {
//        tryGetEntity();
//        return resolve.getClient().startUpdate(entity);
//    }
//
//    private void tryGetEntity() {
//        if (entity == null) {
//            entity = resolve.insert(this);
//        }
//    }
//}
