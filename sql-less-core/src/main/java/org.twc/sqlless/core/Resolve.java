//package org.twc.sqlless.core;
//
//import org.twc.sqlless.core.base.*;
//import org.twc.sqlless.core.statement.Statement;
//import org.twc.sqlless.database.DataBase;
//import org.twc.sqlless.database.Entity;
//import org.twc.sqlless.database.Mysql;
//import org.twc.sqlless.expression.*;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * sql 解析器
// *
// * @author twc
// * @date 2023/11/25
// */
//public interface Resolve<T extends SqlClient<?>> {
//
//    Map<Integer, String> alias =
//            new HashMap<>() {{
//                this.put(0, "a");this.put(1, "b");this.put(2, "c");this.put(3, "d");
//                this.put(4, "e");this.put(5, "f");this.put(6, "g");this.put(7, "h");
//                this.put(8, "i");this.put(9, "j");this.put(10, "k");this.put(11, "l");
//                this.put(12, "m");this.put(13, "n");this.put(14, "o");this.put(15, "p");
//                this.put(16, "q");this.put(17, "r");this.put(18, "s");this.put(19, "t");
//                this.put(20, "u");this.put(21, "v");this.put(22, "w");this.put(23, "x");
//                this.put(24, "y");this.put(25, "z");
//            }};
//
//    static String alias(int index) {
//        return alias.get(index);
//    }
//
//    static Resolve<SqlClient<?>> create(DataBase.Type type, SqlClient<?> sqlClient) {
//        switch (type) {
//            case Mysql:
//            case Oracle:
//                return new Mysql(sqlClient);
//        }
//        throw new UnsupportedOperationException("暂不支持该类型数据库");
//    }
//
//    default void doResolve(Entity entity, IExpression expression, List<?> queryTarget) {
//        if (expression instanceof MappingExpression) {
//            doResolveMappingExpression((MappingExpression) expression, entity, queryTarget);
//        } else if (expression instanceof BinaryExpression) {
//            doResolveBinaryExpression((BinaryExpression) expression, entity, queryTarget);
//        } else if (expression instanceof UnaryExpression) {
//            doResolveUnaryExpression((UnaryExpression) expression, entity, queryTarget);
//        } else if (expression instanceof ValueExpression) {
//            doResolveValueExpression((ValueExpression<?>) expression, entity);
//        } else if (expression instanceof ParensExpression) {
//            doResolveParensExpression((ParensExpression) expression, entity, queryTarget);
//        } else if (expression instanceof ReferenceExpression) {
//            doResolveReferenceExpression((ReferenceExpression) expression, entity, queryTarget);
//        } else if (expression instanceof FieldSelectExpression) {
//            doResolveFieldSelectExpression((FieldSelectExpression) expression, entity, queryTarget);
//        } else if (expression instanceof MethodCallExpression) {
//            doResolveMethodCallExpression((MethodCallExpression) expression, entity, queryTarget);
//        } else if (expression instanceof NewExpression<?>) {
//            doResolveNewExpression((NewExpression<?>) expression, entity, queryTarget);
//        } else if (expression instanceof AssignExpression) {
//            doResolveAssignExpression((AssignExpression) expression, entity, queryTarget);
//        }
//    }
//
//    /*****************************************************************************************************************/
//
//    T getClient();
//
//    /*****************************************************************************************************************/
//
//    Entity query(boolean isDistinct, List<Base> bases, NewExpression<?> newExpression, List<Class<?>> queryClasses, List<?> queryTarget, List<Class<?>> joins);
//
//    Entity cud(Statement<?> statement);
//
//    Entity insert(Statement<?> statement);
//
//    Entity delete(Statement<?> statement);
//
//    Entity update(Statement<?> statement);
//
//    <T> List<Entity> batchSave(List<T> ts);
//
//    void setInsert(Entity entity, SetData setData, List<?> targets);
//
//    void setUpdate(Entity entity, SetData setData, List<?> targets);
//
//    void join(Entity entity, Join join, List<Class<?>> queryClass);
//
//    void select(Entity entity, boolean isDistinct, NewExpression<?> newExpression, List<Class<?>> queryClass, List<?> queryTarget, List<Class<?>> joinClass);
//
//    void where(Entity entity, IExpression expression, List<?> queryTarget);
//
//    void on(Entity entity, IExpression expression, List<?> queryTarget);
//
//    <T> Entity save(T save);
//
//    void take(Entity entity, Take take);
//
//    void skip(Entity entity, Skip skip);
//
//    void orderBy(Entity entity, OrderBy orderBy, List<?> queryTarget);
//
//    /*****************************************************************************************************************/
//
//    void doResolveAssignExpression(AssignExpression assign, Entity entity, List<?> queryTarget);
//
//    void doResolveNewExpression(NewExpression<?> newExpression, Entity entity, List<?> queryTarget);
//
//    void doResolveReferenceExpression(ReferenceExpression reference, Entity entity, List<?> queryTarget);
//
//    void doResolveFieldSelectExpression(FieldSelectExpression fieldSelect, Entity entity, List<?> queryTarget);
//
//    void doResolveMethodCallExpression(MethodCallExpression methodCall, Entity entity, List<?> queryTarget);
//
//    void doResolveMappingExpression(MappingExpression mapping, Entity entity, List<?> queryTarget);
//
//    void doResolveUnaryExpression(UnaryExpression unary, Entity entity, List<?> queryTarget);
//
//    void doResolveValueExpression(ValueExpression<?> value, Entity entity);
//
//    void doResolveBinaryExpression(BinaryExpression binary, Entity entity, List<?> queryTarget);
//
//    void doResolveParensExpression(ParensExpression parens, Entity entity, List<?> queryTarget);
//
//}
