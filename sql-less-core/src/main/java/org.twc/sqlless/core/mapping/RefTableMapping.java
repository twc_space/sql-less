package org.twc.sqlless.core.mapping;


import java.lang.reflect.Method;

public class RefTableMapping extends BaseMapping {
    private final Class<?> target;

    public RefTableMapping(Class<?> target, Method method) {
        super(method);
        this.target = target;
    }

    public Class<?> getTarget() {
        return target;
    }
}
