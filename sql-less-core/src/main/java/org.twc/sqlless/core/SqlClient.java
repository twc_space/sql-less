package org.twc.sqlless.core;

import org.twc.sqlless.core.func.Func0;
import org.twc.sqlless.database.Entity;
import org.twc.sqlless.expression.NewExpression;

import java.util.List;
import java.util.Map;

/**
 * sql 执行器
 *
 * @author twc
 * @date 2023/11/25
 */
public interface SqlClient<T> {

    void setSource(T source);

    T getSource();

    <R> List<R> startQuery(Entity entity, NewExpression<R> newExpression);

    <Key, R> Map<Key, R> startQuery(Entity entity, NewExpression<R> newExpression, Func0<R, Key> getKey);

    <Key, Value, R> Map<Key, Value> startQuery(Entity entity, NewExpression<R> newExpression, Func0<R, Key> getKey, Func0<R, Value> getValue);

    int startUpdate(Entity entity);

    List<Integer> batchUpdate(List<Entity> entityList);

    boolean transactionCud(List<Entity> entityList, Integer transactionIsolation);

}
