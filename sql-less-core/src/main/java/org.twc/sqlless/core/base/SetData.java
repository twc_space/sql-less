package org.twc.sqlless.core.base;

import org.twc.sqlless.expression.IExpression;

public class SetData extends Base {

    private final IExpression expression;

    public SetData(IExpression expression) {
        this.expression = expression;
    }

    public IExpression getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression.toString();
    }

}
