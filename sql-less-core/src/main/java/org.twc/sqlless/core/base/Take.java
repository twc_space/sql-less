package org.twc.sqlless.core.base;


public class Take extends Base {

    private final int count;

    public Take(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

}
