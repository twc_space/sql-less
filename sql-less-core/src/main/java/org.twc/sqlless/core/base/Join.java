package org.twc.sqlless.core.base;

public class Join extends Base {

    private final Type joinType;

    private final Class<?> joinClass;

    public Join(Class<?> joinClass, Type joinType) {
        this.joinClass = joinClass;
        this.joinType = joinType;
    }

    public Type getJoinType() {
        return joinType;
    }

    public Class<?> getJoinClass() {
        return joinClass;
    }

    public enum Type {
        Inner,
        Left,
        Right,
        Full,
    }
}
