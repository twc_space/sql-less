package org.twc.sqlless.core.base;

import org.twc.sqlless.expression.IExpression;

public class Where extends Base {

    private final IExpression expression;

    public Where(IExpression expression) {
        this.expression = expression;
    }

    public IExpression getExpression() {
        return expression;
    }

}
