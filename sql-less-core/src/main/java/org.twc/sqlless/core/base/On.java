package org.twc.sqlless.core.base;

import org.twc.sqlless.expression.IExpression;

public class On extends Base {

    private final IExpression expression;

    public On(IExpression expression) {
        this.expression = expression;
    }

    public IExpression getExpression() {
        return expression;
    }
}
