package org.twc.sqlless.core.base;

import org.twc.sqlless.expression.NewExpression;

public class Select<R> extends Base {

    private final NewExpression<R> newExpression;

    public Select(NewExpression<R> newExpression) {
        this.newExpression = newExpression;
    }

    public NewExpression<R> getNewExpression() {
        return newExpression;
    }

}
