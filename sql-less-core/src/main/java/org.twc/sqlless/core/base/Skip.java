package org.twc.sqlless.core.base;


public class Skip extends Base {
    private final int count;

    public Skip(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
