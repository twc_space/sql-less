package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func10<T1, T2> {
    boolean invoke(T1 t1, T2 t2);
}
