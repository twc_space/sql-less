package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func1000<T1, T2, T3, T4> {
    boolean invoke(T1 t1, T2 t2, T3 t3, T4 t4);
}
