package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func0<T, R> {
    R invoke(T t);
}
