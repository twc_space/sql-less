package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func2<T> {
    void invoke(T t);
}
