package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func100<T1, T2, T3> {
    boolean invoke(T1 t1, T2 t2, T3 t3);
}
