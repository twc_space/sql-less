package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func1<T> {
    boolean invoke(T t);
}
