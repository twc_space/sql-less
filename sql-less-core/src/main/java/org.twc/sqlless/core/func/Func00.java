package org.twc.sqlless.core.func;

@FunctionalInterface
public interface Func00<T1, T2, R> {
    R invoke(T1 t1, T2 t2);
}
