//package org.twc.sqlless.core;
//
//import org.twc.sqlless.core.func.Func2;
//import org.twc.sqlless.core.statement.Statement;
//import org.twc.sqlless.database.Entity;
//
//import java.sql.Connection;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.stream.Collectors;
//
///**
// * 事务 TODO 待完善
// *
// * @author twc
// * @date 2023/11/25
// */
//public class Transaction {
//    public enum Type {
//        None(Connection.TRANSACTION_NONE),
//        ReadUncommitted(Connection.TRANSACTION_READ_UNCOMMITTED),
//        ReadCommitted(Connection.TRANSACTION_READ_COMMITTED),
//        RepeatableRead(Connection.TRANSACTION_REPEATABLE_READ),
//        Serializable(Connection.TRANSACTION_SERIALIZABLE);
//        private final int transactionIsolation;
//
//        Type(int transactionIsolation) {
//            this.transactionIsolation = transactionIsolation;
//        }
//
//        public int getTransactionIsolation() {
//            return transactionIsolation;
//        }
//    }
//
//    private final List<Statement<?>> commands = new ArrayList<>();
//
//    private Type transactionType = null;
//
//    public void push(Statement<?> statement) {
//        commands.add(statement);
//    }
//
//    public List<Statement<?>> getCommands() {
//        return commands;
//    }
//
//    public Transaction setTransactionIsolation(Type transactionType) {
//        this.transactionType = transactionType;
//        return this;
//    }
//
//    public boolean commit() {
//        if (commands.isEmpty()) return false;
//        AtomicBoolean flag = new AtomicBoolean(false);
//        List<Entity> entities = new ArrayList<>(commands.size());
//        Map<Resolve<SqlClient<?>>, List<Statement<?>>> commandMap = commands.stream().collect(Collectors.groupingBy(Statement::getResolve));
//        commandMap.forEach((k, v) -> {
//            for (Statement<?> command : v) {
//                entities.add(command.getResolve().cud(command));
//                flag.set(command.getResolve().getClient().transactionCud(entities, transactionType.getTransactionIsolation()));
//                entities.clear();
//                if (!flag.get()) {//有一条执行失败就失败
//                    break;
//                }
//            }
//        });
//        return flag.get();
//    }
//
//    public void debug(Func2<List<Entity>> func) {
//        List<Entity> entities = new ArrayList<>(commands.size());
//        for (Statement<?> command : commands) {
//            entities.add(command.getResolve().cud(command));
//        }
//        func.invoke(entities);
//    }
//
//}
