package org.twc.sqlless.expression;


public class ValueExpression<T> implements IExpression {
    private final T value;

    ValueExpression(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
