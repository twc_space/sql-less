package org.twc.sqlless.expression;


public class AssignExpression implements IExpression {
    private final IExpression left;
    private final IExpression right;

    AssignExpression(IExpression left, IExpression right) {
        this.left = left;
        this.right = right;
    }

    public IExpression getLeft() {
        return left;
    }

    public IExpression getRight() {
        return right;
    }
}
